# My minimal Vim config

## 🗃️ Contents

This repository contains a very basic selection of plugins:

- [Lightline](https://github.com/itchyny/lightline.vim)
- [Cheatsheet](https://github.com/lifepillar/vim-cheat40)
- modified [hjkl-remap](https://github.com/njcom/hjkl-remap) for **cz-qwertz** layout

and a very simple .vimrc

## 📦 Installation

To install run following command to clone to a `~/.vim` folder:

```bash
git clone https://gitlab.com/RobusTetus/vimconfig.git ~/.vim
```

Then `cd` into cloned directory and initialize and pull submodules:

```bash
git submodules init
git submodules update
```

Lastly make a link of `default-vimrc` to `~/.vimrc`:

```bash
ln default-vimrc ~/.vimrc
```