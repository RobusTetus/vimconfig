" Modified version of https://github.com/njcom/hjkl-remap
" edited for cz-qwertz layout for touch typing (jklů instead of jkl;)

if exists("g:loaded_hjkl_remap") || &cp || v:version < 700
  finish
endif
let g:loaded_hjkl_remap = 1

" Up
noremap <silent> l gk
" Down
noremap <silent> k gj
" Left
noremap <silent> j <Left>
" Right
noremap <silent> ů <Right>

" To window below current one
noremap <C-w>k <C-w>j
noremap <C-w><C-k> <C-w>j

" To window above current one
noremap <C-w>l <C-w>k
noremap <C-w><C-l> <C-w>k

" To window left current one
noremap <C-w>j <C-w>h
noremap <C-w><C-j> <C-w>h

" To window right current one
noremap <C-w>ů <C-w>l
noremap <C-w><C-uring> <C-w>l

" Find next char for `f` or `t`: '
noremap ' ů

" Unmap 'h' key for movement
map h <Nop>
map <C-w>h <Nop>
map <C-w><C-h> <Nop>
